import { Component,ViewChild  } from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  @ViewChild('formcontact', {static: false}) form; // Added this
  contactModel: any = {};
  errormessage:boolean=false
  sucessmessage:boolean=false;

  constructor(public snakbar:MatSnackBar){

  }

  onSubmit() {
    console.log(this.contactModel)
    this.snakbar.open("Message")
    this.form.resetForm()
  }
}
